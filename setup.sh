#!/bin/bash
wget http://hepmc.web.cern.ch/hepmc/releases/hepmc2.06.09.tgz
tar xzf hepmc2.06.09.tgz
cd hepmc2.06.09
./configure --prefix=${PWD}/installed --with-momentum=GEV --with-length=MM
make -j6; make install

cd ..
wget https://lhapdf.hepforge.org/downloads?f=LHAPDF-6.2.3.tar.gz
tar xf downloads?f=LHAPDF-6.2.3.tar.gz
cd LHAPDF-6.2.3
./configure --prefix=${PWD}/installed/
make -j6; make install
wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/nCTEQ15FullNuc_184_74.tar.gz -O- | tar xz -C installed/share/LHAPDF/.

cd ..
wget http://home.thep.lu.se/~torbjorn/pythia8/pythia8244.tgz
tar xzf pythia8244.tgz
cd pythia8244/
./configure --with-hepmc2=${PWD}/../hepmc2.06.09/installed --with-lhapdf6=${PWD}/../LHAPDF-6.2.3/installed
make -j6; make install

cd ..
mv nu_tau_suppression_energies.txt nu_tau_suppression_factor.txt neutrino-tungsten.cmnd neutrino-tungsten-tau.cmnd crosssection.cc crosssectiontau.cc CrossSections.cpp CrossSectionsTau.cpp scan.sh write.py pythia8244/examples/
cd pythia8244/examples

g++ crosssection.cc -o crosssection -I../../hepmc2.06.09/installed/include -I../include -O2  -pedantic -W -Wall -Wshadow -fPIC -L../lib -Wl,-rpath,../lib -lpythia8 -ldl -L../../hepmc2.06.09/installed/lib -Wl,-rpath,../../hepmc2.06.09/installed/lib -lHepMC
g++ crosssectiontau.cc -o crosssectiontau -I../../hepmc2.06.09/installed/include -I../include -O2  -pedantic -W -Wall -Wshadow -fPIC -L../lib -Wl,-rpath,../lib -lpythia8 -ldl -L../../hepmc2.06.09/installed/lib -Wl,-rpath,../../hepmc2.06.09/installed/lib -lHepMC
