#To get Pythia to produce the tau suppression plots:

#Give permissions to the setup.sh script:

chmod 755 setup.sh

#Run the script to setup LHAPDF-6.2.3, HepMC2, and Pythia

./setup.sh

#Give permissions to the scan.sh script:

chmod 755 scan.sh

#Run the script to scan over the energies:

./scan.sh

#Write the cross-sections and uncertainties to files:

python write.py

#Make a TMultiGraph comparing pythia cross sections and the analytical formulae for mu- and tau-neutrinos, respectively:

root CrossSections.cpp
root CrossSectionsTau.cpp
