#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
//#include "LHAPDF-6.2.3/include/LHAPDF/LHAPDF.h"
//R__LOAD_LIBRARY(/afs/cern.ch/user/j/jwspence/WORK/cross-sections-analytical/LHAPDF-6.2.3/installed/lib/libLHAPDF.so);

//#include "Cross_Sections.h"
//#include "Constants.h"
//#include "Interpolate.h"

void CrossSectionsTau(){
     using namespace std;
    	ifstream in("nu_tau_suppression_energies.txt", ifstream::in);
    	Double_t energy[41];
        int i = 0;
    	while((!in.eof()) && in)
    		{
    		float iNumber = 0;
                in >> iNumber;
    		energy[i] = iNumber;
                i += 1;
    		}
    	in.close();
        in.open("SigmaTau.txt",ifstream::in);
        Double_t xstau[41];
        i = 0;
        while ((!in.eof()) && in)
                 {
                 float iFactor = 0;
                 in >> iFactor;
                 xstau[i] = iFactor*1e-27*184;
                 i += 1;
                 }
        in.close();
        in.open("nu_CC_tau_john.txt");
        Double_t xstaujohn[101];
        i = 0;
        while((!in.eof()) && in)
                {
                float ixs = 0;
                in >> ixs;
                xstaujohn[i] = ixs;
                i += 1;
                }
        in.close();
   gSystem->Load("LHAPDF-6.2.3/installed/lib/libLHAPDF.so");
   TCanvas *c1 = new TCanvas("c1","Cross Sections for nu mu",10,1000,800,600);
   c1->SetLogx();
   c1->SetLogy();
   Double_t x[101], y[41],z[101];
   Int_t n1 = 40;
   Int_t n2 = 101;
   for (Int_t j=0;j<101;j++) {
   x[j] = 10*pow(1000,j/100.);
   z[j] = xstau[j]/10/pow(1000,j/100.);
   }
   for (Int_t i=0;i<n1;i++) {
//     cout << energy[i] << endl;
     y[i] = xstaujohn[i]/energy[i];
     cout << y[i] << endl;
//     cout << x[i] << " " << z[i] << " " << endl;
   }
   TGraph* gr = new TGraph(n1,energy,xstau);
   TGraph* cmp = new TGraph(n2,x,xstaujohn);
   gr->SetTitle("Pythia");
   cmp->SetTitle("Reference cross sections");
   cmp->SetLineColor(4);
   gr->Draw("AC*");
   TMultiGraph *mg = new TMultiGraph();
   mg->Add(gr);
   mg->Add(cmp);
   mg->Draw("a");
   mg->SetTitle("Tau-Neutrino Cross Sections vs. Energy (Comparison)");
   mg->GetXaxis()->SetTitle("Energy (GeV)");
   mg->GetYaxis()->SetTitle("Total Cross Section (cm^2)");
   c1->BuildLegend();
   c1->Print("NuTau.pdf");
}
